package main

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/unrolled/render"
)

// WS global variable for sync.Mutex
var WS map[string]WSconn = make(map[string]WSconn)

// WebWS website ws
var WebWS = []WSconn{}

// ConsoleWS console ws
var ConsoleWS = []WSconn{}

// SQL sql reference
var SQL *sql.DB

// JWTtoken for JWT auth
var JWTtoken string

// AccessToken map
var AccessToken map[string]bool = make(map[string]bool)

const local string = "LOCAL"

func main() {
	var (
		// environment variables
		env        = os.Getenv("ENV")     // LOCAL, DEV, STG, PRD
		port       = os.Getenv("PORT")    // server traffic on this port
		version    = os.Getenv("VERSION") // path to VERSION file
		connection = os.Getenv("DATABASE")
	)
	JWTtoken = os.Getenv("JWTTOKEN")

	if env == "" || env == local {
		// running from localhost, so set some default values
		env = local
		port = "8080"
		version = "VERSION"
		connection = "tsacbus:5XvecSUDU6eGCsa8@tcp(www.tsatech.nz:3306)/tsa-cbus?parseTime=true"
		JWTtoken = "Yuy37Y98s85vspBY0PsOcxFSuLvLts2x"
	}
	// reading version from file
	version, err := ParseVersionFile(version)
	if err != nil {
		log.Fatal(err)
	}

	// setup ws
	connected := false

	// setup
	SQL, err = sql.Open("mysql", connection)
	if err != nil {
		log.Fatal(err)
	}
	defer SQL.Close()

	err = DBGetAllTokens()
	if err != nil {
		log.Fatal(err)
	}

	// initialse application context
	ctx := AppContext{
		Render:    render.New(),
		Version:   version,
		Env:       env,
		Port:      port,
		Connected: &connected,
	}
	// start application
	StartServer(ctx)
}
