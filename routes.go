package main

// Route is the model for the router setup
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc HandlerFunc
}

// Routes are the main setup for our Router
type Routes []Route

var routes = Routes{
	//Unauthorised
	Route{"GET Healthcheck", "GET", "/healthcheck", HealthcheckHandler},
	Route{"GET Setup Client", "GET", "/setupclient", GetClientConfigHandler},
	Route{"GET Discovery", "GET", "/discovery", AlexaDiscoveryHandler},

	//Alexa Bearer
	Route{"POST Light", "POST", "/light", BearerAuthMiddleware(LightHandler)},
	Route{"POST Curtain", "POST", "/curtain", BearerAuthMiddleware(CurtainHandler)},
	Route{"POST Dimmer", "POST", "/dimmer", BearerAuthMiddleware(DimmerHandler)},
	Route{"GET ReportState", "GET", "/reportstate", BearerAuthMiddleware(ReportStateHandler)},

	//Auth with catergory
	Route{"GET WebSockets", "GET", "/ws/{auth}", wsEndpoint},

	//Website
	Route{"GET Devices", "GET", "/device", TokenAuthMiddleware(GetDevicesHandler)},
	Route{"PUT Devices", "PUT", "/device", TokenAuthMiddleware(PutDeviceHandler)},
	Route{"GET Devices", "GET", "/device/{id:[0-9]+}", TokenAuthMiddleware(GetDeviceHandler)},
	Route{"POST Devices", "POST", "/device/{id:[0-9]+}", TokenAuthMiddleware(PostDeviceHandler)},

	Route{"GET Rooms", "GET", "/room", TokenAuthMiddleware(GetRoomsHandler)},
	Route{"PUT Room", "PUT", "/room", NotImplementedHandler},
	Route{"POST Rooms", "POST", "/device/{id:[0-9]+}", TokenAuthMiddleware(GetRoomHandler)},

	Route{"POST Discovery", "POST", "/discovery", TokenAuthMiddleware(PostDiscoveryHandler)},

	Route{"GET Alexa", "GET", "/alexa", TokenAuthMiddleware(NotImplementedHandler)},
	Route{"GET NewAlexa", "GET", "/newalexa", TokenAuthMiddleware(NotImplementedHandler)},

	Route{"POST LoginHandler", "POST", "/login", LoginHandler},
	Route{"GET LogoutHandler", "GET", "/logout", TokenAuthMiddleware(LogoutHandler)},
	Route{"POST CreateUserHandler", "POST", "/createuser", TokenAuthMiddleware(CreateUserHandler)},
	Route{"GET GetUsersHandler", "GET", "/user", TokenAuthMiddleware(GetUsersHandler)},

	Route{"GET ConsoleWS", "GET", "/console", TokenAuthMiddleware(wsConsole)},
	Route{"GET WebWS", "GET", "/webws", TokenAuthMiddleware(wsWeb)},
}
