package main

import (
	"log"
	"sync"

	"github.com/gorilla/websocket"
)

//Config config
type Config struct {
	Cbus struct {
		Address string `json:"address"`
		Port    string `json:"port"`
		Status  string `json:"status"`
	} `json:"cbus"`
	SSL    bool   `json:"ssl"`
	Server string `json:"server"`
	Path   string `json:"path"`
	WSpath string `json:"wspath"`
	Token  string `json:"token"`
}

// DevicePacket packet for lighting
type DevicePacket struct {
	Device string `json:"device"`
	State  string `json:"state"`
}

// DimmerPacket packet for lighting
type DimmerPacket struct {
	Device string `json:"device"`
	Level  string `json:"level"`
}

// ReportState packet for reporting state
type ReportState struct {
	Type  string `json:"type"`
	State string `json:"state"`
}

// WSpacket packet for ws
type WSpacket struct {
	Type    string `json:"t"`
	Message string `json:"m"`
}

// WSconn connection for ws using mutex
type WSconn struct {
	Socket *websocket.Conn // websocket connection of the player
	mu     *sync.Mutex
}

// Send WSconn function to send using go concurrency
func (conn WSconn) Send(p WSpacket, ctx AppContext) error {
	conn.mu.Lock()
	defer conn.mu.Unlock()
	err := conn.Socket.WriteJSON(p)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

// Device data for db
type Device struct {
	ID            int64  `json:"device"`
	EndpointID    string `json:"endpointid"`
	FriendlyName  string `json:"friendlyname"`
	Description   string `json:"description"`
	Categories    string `json:"categories"`
	ProjectName   string `json:"projectname"`
	CNIAddress    int64  `json:"cniaddress"`
	DeviceAddress int64  `json:"deviceaddress"`
	Discovery     bool   `json:"discovery"`
	Status        string `json:"status"`
	RoomID        int64  `json:"roomid"`
}

// Room data
type Room struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

// Alexa data
type Alexa struct {
	ID     int64  `json:"id"`
	RoomID int64  `json:"roomid"`
	Token  string `json:"token"`
}

// User data
type User struct {
	ID       int64  `json:"id"`
	RoomID   int64  `json:"roomid"`
	Username string `json:"username"`
	Password string `json:"password"`
	Token    string `json:"token"`
	IsAdmin  bool   `json:"isadmin"`
}

// Login Used for logining
type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// EditUser Used for editing user
type EditUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
	RoomID   int64  `json:"roomid"`
}

// PostDiscovery used for changing discovery
type PostDiscovery struct {
	Device int64 `json:"device"`
	State  bool  `json:"state"`
}

// Commands for webws
type Commands struct {
	Status        string `json:"status"`
	ProjectName   string `json:"projectname"`
	CNIAddress    string `json:"cniaddress"`
	DeviceAddress string `json:"deviceaddress"`
}
