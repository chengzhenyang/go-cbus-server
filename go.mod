module go-cbus-server

go 1.12

require (
	github.com/codegangsta/negroni v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.1
	github.com/myesui/uuid v1.0.0 // indirect
	github.com/palantir/stacktrace v0.0.0-20161112013806-78658fd2d177
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/twinj/uuid v1.0.0
	github.com/unrolled/render v1.0.2
	github.com/unrolled/secure v1.0.7
	google.golang.org/appengine v1.6.5
)
