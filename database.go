package main

import "github.com/twinj/uuid"

// Local Cache for instant response

// LightsMap map of all lights
var LightsMap map[string]string

// CurtainMap map of all curtains
var CurtainMap map[string]string

// AlexaMap map of all alexa token and room
var AlexaMap map[string]string

// DBAddUser to add new user
func DBAddUser(user EditUser) (string, error) {
	uuid := uuid.NewV4().String()
	_, err := SQL.Exec("INSERT INTO `user` (`id`, `roomid`, `username`, `password`, `token`, `defaultproject`, `isadmin`) VALUES (NULL, ?, ?, ?,?,?, 0)", user.RoomID, user.Username, EncryptPassword(user.Password), uuid, "")
	return uuid, err
}

// DBGetUsers to get all users
func DBGetUsers() ([]User, error) {
	rows, err := SQL.Query("SELECT `id`,`roomid`,`username`,`password`,`token`,`isadmin` FROM `user`")
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	var users []User
	for rows.Next() {
		var user User
		rows.Scan(&user.ID, &user.RoomID, &user.Username, &user.Password, &user.Token, &user.IsAdmin)
		users = append(users, user)
	}
	return users, nil
}

//DBGetAllTokens to get all tokens
func DBGetAllTokens() error {
	rows, err := SQL.Query("SELECT `token` FROM `user`")
	defer rows.Close()
	if err != nil {
		return err
	}
	for rows.Next() {
		var token string
		rows.Scan(&token)
		AccessToken[token] = true
	}
	return nil
}

//DBEditRoom to edit existing user
func DBEditRoom(user EditUser) error {
	return nil
}

//DBLogin get password from username
func DBLogin(username string) (string, int64, bool, error) {
	var password string
	var roomid int64
	var isadmin bool
	row := SQL.QueryRow("SELECT `password`, `roomid`, `isadmin` FROM `user` WHERE `username`=?", username)
	err := row.Scan(&password, &roomid, &isadmin)
	if err != nil {
		return "", 0, false, err
	}
	return password, roomid, isadmin, nil
}

//DBGetAllDevices get all devices
func DBGetAllDevices() ([]Device, error) {
	rows, err := SQL.Query("SELECT `id`,`endpointid`,`friendlyname`,`description`,`categories`,`projectname`,`cniaddress`,`deviceaddress`,`discovery`,`status`,`roomid` FROM `devices`")
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	var devices []Device
	for rows.Next() {
		var device Device
		rows.Scan(&device.ID, &device.EndpointID, &device.FriendlyName, &device.Description, &device.Categories, &device.ProjectName, &device.CNIAddress, &device.DeviceAddress, &device.Discovery, &device.Status, &device.RoomID)
		devices = append(devices, device)
	}
	return devices, nil
}

//DBGetDevice get one device
func DBGetDevice(id int64) (Device, error) {
	row := SQL.QueryRow("SELECT `id`,`endpointid`,`friendlyname`,`description`,`categories`,`projectname`,`cniaddress`,`deviceaddress`,`discovery`,`status`,`roomid` FROM `devices` WHERE `id`=?", id)
	var device Device
	err := row.Scan(&device.ID, &device.EndpointID, &device.FriendlyName, &device.Description, &device.Categories, &device.ProjectName, &device.CNIAddress, &device.DeviceAddress, &device.Discovery, &device.Status, &device.RoomID)
	return device, err
}

//DBPutDevice add a device
func DBPutDevice(device Device) error {
	query := "INSERT INTO `devices` (`id`, `endpointid`, `friendlyname`, `description`, `categories`, `projectname`, `cniaddress`, `deviceaddress`, `discovery`, `status`, `roomid`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?)"
	_, err := SQL.Exec(query, device.EndpointID, device.FriendlyName, device.Description, device.Categories, device.ProjectName, device.CNIAddress, device.DeviceAddress, device.Discovery, device.RoomID)
	return err
}

//DBPostDevice edit a device
func DBPostDevice(device Device) error {
	insert := ""
	if device.EndpointID != "" {
		insert = insert + " `endpointid` = " + string(device.EndpointID) + ","
	}
	if device.FriendlyName != "" {
		insert = insert + " `friendlyname` = " + string(device.CNIAddress) + ","
	}
	if device.Description != "" {
		insert = insert + " `description` = " + string(device.Description) + ","
	}
	if device.Categories != "" {
		insert = insert + " `categories` = " + string(device.Categories) + ","
	}
	if device.ProjectName != "" {
		insert = insert + " `projectname` = " + string(device.ProjectName) + ","
	}
	if device.CNIAddress != 0 {
		insert = insert + " `cniaddress` = " + string(device.CNIAddress) + ","
	}
	if device.DeviceAddress != 0 {
		insert = insert + " `deviceaddress` = " + string(device.DeviceAddress) + ","
	}
	if device.RoomID != 0 {
		insert = insert + " `roomid` = " + string(device.RoomID) + ","
	}
	insert = insert + " `discovery` = ?"
	query := "UPDATE `devices` SET" + " WHERE `devices`.`id` = ?"
	_, err := SQL.Exec(query, device.Discovery, device.ID)
	return err
}

//DBGetAllRooms get all rooms
func DBGetAllRooms() ([]Room, error) {
	rows, err := SQL.Query("SELECT `id`,`name` FROM `rooms`")
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	var rooms []Room
	for rows.Next() {
		var room Room
		rows.Scan(&room.ID, &room.Name)
		rooms = append(rooms, room)
	}
	return rooms, nil
}

//DBGetRoom get all room
func DBGetRoom(id int64) (Room, error) {
	row := SQL.QueryRow("SELECT `id`,`name` FROM `rooms` WHERE `id`=?", id)
	var room Room
	err := row.Scan(&room.ID, &room.Name)
	return room, err
}
