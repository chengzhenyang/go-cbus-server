package main

import (
	"fmt"
	"log"
)

// CbusCurtain control lighting using websockets
func CbusCurtain(device string, state string, ctx AppContext) error {

	var projectname string
	var cniaddress, deviceaddress int64
	row := SQL.QueryRow("SELECT `projectname`,`cniaddress`,`deviceaddress` FROM `devices` WHERE `categories`='INTERIOR_BLIND' AND `endpointid`=?", device)
	err := row.Scan(&projectname, &cniaddress, &deviceaddress)
	if err != nil {
		return err
	}
	if projectname == "INTCON" {
		state = "INTCON"
	}
	log.Println(state)
	return WSCurtainsend(state, fmt.Sprintf("//%s/%d/56/%d", projectname, cniaddress, deviceaddress), ctx)
}

// WSCurtainsend to ws using address and return error
func WSCurtainsend(state string, address string, ctx AppContext) error {
	level := ""
	switch state {
	case "Position.Open":
		level = "255"
	case "Position.Close":
		level = "0"
	case "Position.Stop":
		level = "5"
	case "INTCON":
		level = "248"
	}
	for auth := range WS {
		err := WS[auth].Send(WSpacket{Type: "c", Message: "set " + address + " level " + level}, ctx)
		if err != nil {
			return err
		}
	}
	return nil
}
