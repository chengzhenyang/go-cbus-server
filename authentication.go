package main

import (
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Claims for JWT
type Claims struct {
	RoomID  int64
	isAdmin bool
	jwt.StandardClaims
}

// JWTcreate Create JWT token
func JWTcreate(RoomID int64, isAdmin bool) (string, error) {
	expirationTime := time.Now().Add(24 * time.Hour)
	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		RoomID:  RoomID,
		isAdmin: isAdmin,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}
	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString([]byte(JWTtoken))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// JWTverify Verify Token
func JWTverify(tknStr string) error {
	claims := &Claims{}
	tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(JWTtoken), nil
	})
	if err != nil {
		return err
	}
	if !tkn.Valid {
		return errors.New("bad token")
	}
	return nil
}

// EncryptPassword with sha256
func EncryptPassword(password string) string {
	h := sha256.Sum256([]byte(password))
	return base64.StdEncoding.EncodeToString(h[:])
}

// Auth check token
func Auth(r *http.Request) bool {
	c, err := r.Cookie("token")
	if err != nil {
		if err == http.ErrNoCookie {
			return false
		}
		return false
	}
	err = JWTverify(c.Value)
	if err != nil {
		return false
	}
	return true
}

// Bearer check bearer token
func Bearer(r *http.Request) bool {
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	if len(splitToken) != 2 {
		return false
	}
	if _, ok := AccessToken[splitToken[1]]; !ok {
		return false
	}
	return true
}

// BearerAuthMiddleware auth with bearer token
func BearerAuthMiddleware(fn func(http.ResponseWriter, *http.Request, AppContext)) func(http.ResponseWriter, *http.Request, AppContext) {
	return func(w http.ResponseWriter, r *http.Request, ctx AppContext) {
		if !Bearer(r) {
			UnauthorisedHandler(w, r, ctx)
			return
		}
		fn(w, r, ctx)
	}
}

// TokenAuthMiddleware auth with token cookie
func TokenAuthMiddleware(fn func(http.ResponseWriter, *http.Request, AppContext)) func(http.ResponseWriter, *http.Request, AppContext) {
	return func(w http.ResponseWriter, r *http.Request, ctx AppContext) {
		if !Auth(r) {
			UnauthorisedHandler(w, r, ctx)
			return
		}
		fn(w, r, ctx)
	}
}

// UnauthorisedHandler return
func UnauthorisedHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	ctx.Render.JSON(w, http.StatusUnauthorized, "Not logged in")
	return
}
