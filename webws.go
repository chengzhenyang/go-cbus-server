package main

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"
)

func wsConsole(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	// upgrade this connection to a WebSocket
	// connection
	var err error
	var temp WSconn
	temp.Socket, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	temp.mu = &sync.Mutex{}
	ConsoleWS = append(ConsoleWS, temp)
	log.Println("Console Connection Connected")
	temp.Send(WSpacket{Type: "c", Message: "Console Connection Connected"}, ctx)
}

func wsWeb(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	// upgrade this connection to a WebSocket
	// connection
	var err error
	var temp WSconn
	temp.Socket, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	temp.mu = &sync.Mutex{}
	WebWS = append(WebWS, temp)
	log.Println("Web Connection Connected")
	temp.Send(WSpacket{Type: "c", Message: "Console Connection Connected"}, ctx)
}

// wsConsoleSend send string to console
func wsConsoleSend(text string, ctx AppContext) {
	for _, ws := range ConsoleWS {
		err := ws.Send(WSpacket{Type: "c", Message: text}, ctx)
		if err != nil {
			ws.Socket.Close()
		}
	}
}

// wsWebSend send json
func wsWebSend(data Commands, ctx AppContext) {
	jsonWS, _ := json.Marshal(data)
	for _, ws := range WebWS {
		err := ws.Send(WSpacket{Type: "c", Message: string(jsonWS)}, ctx)
		if err != nil {
			ws.Socket.Close()
		}
	}
}
