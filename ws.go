package main

import (
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{}

func wsEndpoint(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	auth := mux.Vars(r)["auth"]
	if auth == "" {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}
	if _, ok := AccessToken[auth]; !ok {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Bad Token")
		return
	}
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	// upgrade this connection to a WebSocket
	// connection
	var err error
	var temp WSconn

	temp.Socket, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	temp.mu = &sync.Mutex{}
	WS[auth] = temp

	var defaultproject string
	row := SQL.QueryRow("SELECT `defaultproject` FROM `user` WHERE `token`=?", auth)
	row.Scan(&defaultproject)

	log.Println("Client Connecting")
	*ctx.Connected = true
	err = WS[auth].Send(WSpacket{Type: "c", Message: "project load " + defaultproject}, ctx)
	if err != nil {
		log.Println(err)
	}
	err = WS[auth].Send(WSpacket{Type: "c", Message: "project use " + defaultproject}, ctx)
	if err != nil {
		log.Println(err)
	}
	err = WS[auth].Send(WSpacket{Type: "c", Message: "net open 254"}, ctx)
	if err != nil {
		log.Println(err)
	}
	// listen indefinitely for new messages coming
	// through on our WebSocket connection
	log.Println("Client Connected")
	go ping(ctx, auth)
	reader(ctx, auth)
}

func reader(ctx AppContext, auth string) {
	for {
		// read in a message
		var wsjson WSpacket
		err := WS[auth].Socket.ReadJSON(&wsjson)
		if err != nil {
			log.Println(err)
			log.Println("WS Closed")
			if _, ok := WS[auth]; ok {
				delete(WS, auth)
			}
			if len(WS) == 0 {
				log.Println("zero devices are connected")
				*ctx.Connected = false
			}
			break
		}
		// print out that message for clarity
		if wsjson.Type == "c" {
			log.Println("telcontrol: " + wsjson.Message)
		}
		if wsjson.Type == "s" {
			log.Println("telstatus: " + wsjson.Message)
			status := strings.Fields(wsjson.Message)
			if status[0] == "lighting" {
				log.Println(status)
				if status[1] == "on" || status[1] == "off" || status[1] == "ramp" {
					address := strings.Split(status[2], "/")
					var reverse bool
					row := SQL.QueryRow("SELECT `categories` FROM `devices` WHERE `projectname`=? AND `cniaddress`=? AND `deviceaddress`=?", address[2], address[3], address[5])
					categories := ""
					row.Scan(&categories)
					go wsConsoleSend(address[2]+": "+wsjson.Message, ctx)

					if categories == "DIMMER" {
						switch strings.ToUpper(status[1]) {
						case "ON":
							status[1] = "100"
						case "OFF":
							status[1] = "0"
						case "RAMP":
							level, _ := strconv.Atoi(status[3])
							status[1] = strconv.Itoa(int(float64(level) / 254 * 100))
						}
					}
					if categories == "INTERIOR_BLIND" {
						switch strings.ToUpper(status[1]) {
						case "ON":
							status[1] = "Position.Open"
						case "OFF":
							status[1] = "Position.Close"
						case "RAMP":
							status[1] = "Position.Stop"
						}
					}
					if reverse {
						switch strings.ToUpper(status[1]) {
						case "ON":
							status[1] = "OFF"
						case "OFF":
							status[1] = "ON"
						}
					}
					query := "UPDATE `devices` SET `status` = ? WHERE `projectname`=? AND `cniaddress`=? AND `deviceaddress`=?"
					_, err = SQL.Exec(query, strings.ToUpper(status[1]), address[2], address[3], address[5])
					go wsWebSend(Commands{strings.ToUpper(status[1]), address[2], address[3], address[5]}, ctx)
					if err != nil {
						log.Println(err)
					}
				}
			}
		}
	}
}

func ping(ctx AppContext, auth string) {
	for {
		if _, ok := WS[auth]; ok {
			wsjson := WSpacket{Type: "ping", Message: ""}
			WS[auth].Send(wsjson, ctx)
			time.Sleep(5 * time.Second)
		} else {
			log.Println("Stopping Ping")
			break
		}
	}
}
