package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// AlexaDevice array of discovery
type AlexaDevice struct {
	EndpointID        string             `json:"endpointId"`
	FriendlyName      string             `json:"friendlyName"`
	Description       string             `json:"description"`
	ManufacturerName  string             `json:"manufacturerName"`
	DisplayCategories []string           `json:"displayCategories"`
	Capabilities      []AlexaCapabilites `json:"capabilities"`
}

// AlexaCapabilites array of Capability
type AlexaCapabilites struct {
	Type       string `json:"type"`
	Interface  string `json:"interface"`
	Instance   string `json:"instance,omitempty"`
	Version    string `json:"version"`
	Properties *struct {
		Supported []struct {
			Name string `json:"name"`
		} `json:"supported"`
		ProactivelyReported bool `json:"proactivelyReported"`
		Retrievable         bool `json:"retrievable"`
		NonControllable     bool `json:"nonControllable"`
	} `json:"properties,omitempty"`
	CapabilityResources *struct {
		FriendlyNames []struct {
			Type  string `json:"@type"`
			Value struct {
				AssetID string `json:"assetId"`
			} `json:"value"`
		} `json:"friendlyNames"`
	} `json:"capabilityResources,omitempty"`
	Configuration *AlexaConfig    `json:"configuration,omitempty"`
	Semantics     *AlexaSemantics `json:"semantics,omitempty"`
}

// AlexaSemantics Semantics
type AlexaSemantics struct {
	ActionMappings []AlexaActionMappings `json:"actionMappings"`
	StateMappings  []struct {
		Type   string   `json:"@type"`
		States []string `json:"states"`
		Value  string   `json:"value"`
	} `json:"stateMappings"`
}

// AlexaActionMappings struct
type AlexaActionMappings struct {
	Type      string   `json:"@type"`
	Actions   []string `json:"actions"`
	Directive struct {
		Name    string `json:"name"`
		Payload struct {
			Mode string `json:"mode"`
		} `json:"payload"`
	} `json:"directive"`
}

// AlexaConfig for config
type AlexaConfig struct {
	Ordered        bool                  `json:"ordered"`
	SupportedModes []AlexaSupportedModes `json:"supportedModes"`
}

// AlexaSupportedModes struct
type AlexaSupportedModes struct {
	Value         string `json:"value"`
	ModeResources struct {
		FriendlyNames []AlexaFriendlyNames `json:"friendlyNames"`
	} `json:"modeResources"`
}

// AlexaFriendlyNames struct
type AlexaFriendlyNames struct {
	Type  string `json:"@type"`
	Value struct {
		Text    string `json:"text,omitempty"`
		Locale  string `json:"locale,omitempty"`
		AssetID string `json:"assetId,omitempty"`
	} `json:"value,omitempty"`
}

// AlexaDiscoveryHandler for discovery
func AlexaDiscoveryHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	rows, err := SQL.Query("SELECT `endpointid`,`friendlyname`,`description`,`categories` FROM `devices` WHERE `discovery` = true")
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	var lightcapabilites = []AlexaCapabilites{
		AlexaCapabilites{
			Type:      "AlexaInterface",
			Interface: "Alexa",
			Version:   "3",
		},
		AlexaCapabilites{
			Type:      "AlexaInterface",
			Interface: "Alexa.PowerController",
			Version:   "3",
			Properties: &struct {
				Supported []struct {
					Name string `json:"name"`
				} `json:"supported"`
				ProactivelyReported bool `json:"proactivelyReported"`
				Retrievable         bool `json:"retrievable"`
				NonControllable     bool `json:"nonControllable"`
			}{
				Supported: []struct {
					Name string `json:"name"`
				}{{
					Name: "powerState",
				}},
				ProactivelyReported: false,
				Retrievable:         true,
			},
		},
	}

	var dimmercapabilites = []AlexaCapabilites{
		AlexaCapabilites{
			Type:      "AlexaInterface",
			Interface: "Alexa",
			Version:   "3",
		},
		AlexaCapabilites{
			Type:      "AlexaInterface",
			Interface: "Alexa.BrightnessController",
			Version:   "3",
			Properties: &struct {
				Supported []struct {
					Name string `json:"name"`
				} `json:"supported"`
				ProactivelyReported bool `json:"proactivelyReported"`
				Retrievable         bool `json:"retrievable"`
				NonControllable     bool `json:"nonControllable"`
			}{
				Supported: []struct {
					Name string `json:"name"`
				}{{
					Name: "brightness",
				}},
				ProactivelyReported: false,
				Retrievable:         true,
			},
		},
		AlexaCapabilites{
			Type:      "AlexaInterface",
			Interface: "Alexa.PowerController",
			Version:   "3",
			Properties: &struct {
				Supported []struct {
					Name string `json:"name"`
				} `json:"supported"`
				ProactivelyReported bool `json:"proactivelyReported"`
				Retrievable         bool `json:"retrievable"`
				NonControllable     bool `json:"nonControllable"`
			}{
				Supported: []struct {
					Name string `json:"name"`
				}{{
					Name: "powerState",
				}},
				ProactivelyReported: false,
				Retrievable:         true,
			},
		},
	}

	var curtaincapabilites = []AlexaCapabilites{
		AlexaCapabilites{
			Type:      "AlexaInterface",
			Interface: "Alexa",
			Version:   "3",
		},
		AlexaCapabilites{
			Type:      "AlexaInterface",
			Interface: "Alexa.ModeController",
			Instance:  "Curtain.Position",
			Version:   "3",
			Properties: &struct {
				Supported []struct {
					Name string `json:"name"`
				} `json:"supported"`
				ProactivelyReported bool `json:"proactivelyReported"`
				Retrievable         bool `json:"retrievable"`
				NonControllable     bool `json:"nonControllable"`
			}{
				Supported: []struct {
					Name string `json:"name"`
				}{{
					Name: "mode",
				}},
				ProactivelyReported: false,
				Retrievable:         true,
			},
			CapabilityResources: &struct {
				FriendlyNames []struct {
					Type  string `json:"@type"`
					Value struct {
						AssetID string `json:"assetId"`
					} `json:"value"`
				} `json:"friendlyNames"`
			}{
				FriendlyNames: []struct {
					Type  string `json:"@type"`
					Value struct {
						AssetID string `json:"assetId"`
					} `json:"value"`
				}{{
					Type: "asset",
					Value: struct {
						AssetID string `json:"assetId"`
					}{AssetID: "Alexa.Setting.Opening"},
				}},
			},
			Configuration: &AlexaConfig{
				SupportedModes: []AlexaSupportedModes{
					AlexaSupportedModes{
						Value: "Position.Open",
						ModeResources: struct {
							FriendlyNames []AlexaFriendlyNames `json:"friendlyNames"`
						}{
							FriendlyNames: []AlexaFriendlyNames{
								AlexaFriendlyNames{
									Type: "text",
									Value: struct {
										Text    string `json:"text,omitempty"`
										Locale  string `json:"locale,omitempty"`
										AssetID string `json:"assetId,omitempty"`
									}{
										Text:   "Opening",
										Locale: "en-AU",
									},
								},
								AlexaFriendlyNames{
									Type: "asset",
									Value: struct {
										Text    string `json:"text,omitempty"`
										Locale  string `json:"locale,omitempty"`
										AssetID string `json:"assetId,omitempty"`
									}{
										AssetID: "Alexa.Value.Open",
									},
								},
							},
						},
					},
					AlexaSupportedModes{
						Value: "Position.Stop",
						ModeResources: struct {
							FriendlyNames []AlexaFriendlyNames `json:"friendlyNames"`
						}{
							FriendlyNames: []AlexaFriendlyNames{
								AlexaFriendlyNames{
									Type: "text",
									Value: struct {
										Text    string `json:"text,omitempty"`
										Locale  string `json:"locale,omitempty"`
										AssetID string `json:"assetId,omitempty"`
									}{
										Text:   "Stop",
										Locale: "en-AU",
									},
								},
								AlexaFriendlyNames{
									Type: "text",
									Value: struct {
										Text    string `json:"text,omitempty"`
										Locale  string `json:"locale,omitempty"`
										AssetID string `json:"assetId,omitempty"`
									}{
										Text:   "Pause",
										Locale: "en-AU",
									},
								},
							},
						},
					},
					AlexaSupportedModes{
						Value: "Position.Close",
						ModeResources: struct {
							FriendlyNames []AlexaFriendlyNames `json:"friendlyNames"`
						}{
							FriendlyNames: []AlexaFriendlyNames{
								AlexaFriendlyNames{
									Type: "text",
									Value: struct {
										Text    string `json:"text,omitempty"`
										Locale  string `json:"locale,omitempty"`
										AssetID string `json:"assetId,omitempty"`
									}{
										Text:   "Closed",
										Locale: "en-AU",
									},
								},
								AlexaFriendlyNames{
									Type: "asset",
									Value: struct {
										Text    string `json:"text,omitempty"`
										Locale  string `json:"locale,omitempty"`
										AssetID string `json:"assetId,omitempty"`
									}{
										AssetID: "Alexa.Value.Close",
									},
								},
							},
						},
					},
				},
			},
			Semantics: &AlexaSemantics{
				ActionMappings: []AlexaActionMappings{
					AlexaActionMappings{
						Type: "ActionsToDirective",
						Actions: []string{
							"Alexa.Actions.Close",
							"Alexa.Actions.Lower",
						},
						Directive: struct {
							Name    string `json:"name"`
							Payload struct {
								Mode string `json:"mode"`
							} `json:"payload"`
						}{
							Name: "SetMode",
							Payload: struct {
								Mode string `json:"mode"`
							}{
								Mode: "Position.Close",
							},
						},
					},
					AlexaActionMappings{
						Type: "ActionsToDirective",
						Actions: []string{
							"Alexa.Actions.Open",
							"Alexa.Actions.Raise",
						},
						Directive: struct {
							Name    string `json:"name"`
							Payload struct {
								Mode string `json:"mode"`
							} `json:"payload"`
						}{
							Name: "SetMode",
							Payload: struct {
								Mode string `json:"mode"`
							}{
								Mode: "Position.Open",
							},
						},
					},
				},
				StateMappings: []struct {
					Type   string   `json:"@type"`
					States []string `json:"states"`
					Value  string   `json:"value"`
				}{
					struct {
						Type   string   `json:"@type"`
						States []string `json:"states"`
						Value  string   `json:"value"`
					}{
						Type:   "StatesToValue",
						States: []string{"Alexa.States.Closed"},
						Value:  "Position.Close",
					},
					struct {
						Type   string   `json:"@type"`
						States []string `json:"states"`
						Value  string   `json:"value"`
					}{
						Type:   "StatesToValue",
						States: []string{"Alexa.States.Open"},
						Value:  "Position.Open",
					},
				},
			},
		},
	}

	defer rows.Close()
	var devices []AlexaDevice
	for rows.Next() {
		var device AlexaDevice
		var categories string
		rows.Scan(&device.EndpointID, &device.FriendlyName, &device.Description, &categories)
		device.ManufacturerName = "TSA Tech Limited"
		device.DisplayCategories = append(device.DisplayCategories, categories)
		switch categories {
		case "LIGHT":
			device.Capabilities = lightcapabilites
			break
		case "OTHER":
			device.Capabilities = lightcapabilites
			break
		case "DIMMER":
			device.DisplayCategories = []string{"LIGHT"}
			device.Capabilities = dimmercapabilites
			break
		case "INTERIOR_BLIND":
			device.Capabilities = curtaincapabilites
			break
		}
		devices = append(devices, device)
	}
	ctx.Render.JSON(w, http.StatusOK, devices)
}

// PostDiscoveryHandler for change discovery
func PostDiscoveryHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	var data PostDiscovery
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	log.Println(err)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}
	_, err = SQL.Exec("UPDATE `devices` SET `discovery` = ? WHERE `devices`.`id` = ?", data.State, data.Device)
	log.Println(err)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Device not found")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, "Done")
}
