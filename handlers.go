package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/twinj/uuid"
)

// HandlerFunc is a custom implementation of the http.HandlerFunc
type HandlerFunc func(http.ResponseWriter, *http.Request, AppContext)

// makeHandler allows us to pass an environment struct to our handlers, without resorting to global
// variables. It accepts an environment (Env) struct and our own handler function. It returns
// a function of the type http.HandlerFunc so can be passed on to the HandlerFunc in main.go.
func makeHandler(ctx AppContext, fn func(http.ResponseWriter, *http.Request, AppContext)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		origin := r.Header.Get("Origin")
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Accept, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		fn(w, r, ctx)
	}
}

// HealthcheckHandler returns useful info about the app
func HealthcheckHandler(w http.ResponseWriter, req *http.Request, ctx AppContext) {
	check := Healthcheck{
		AppName: "TSA-go-cbus-server",
		Version: ctx.Version,
	}
	ctx.Render.JSON(w, http.StatusOK, check)
}

// HomePageHandler set as root handler
func HomePageHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	fmt.Fprintf(w, "TSA-go-cbus-server")
}

// LightHandler used to set light status
func LightHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	if !*ctx.Connected {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Not connected")
		return
	}
	var data DevicePacket
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil || data.Device == "" || data.State == "" {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}

	if data.State != "ON" && data.State != "OFF" {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}

	err = CbusLight(data.Device, data.State, ctx)

	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid device")
		return
	}

	ctx.Render.JSON(w, http.StatusOK, Status{Status: "200", Message: data.Device})
}

// DimmerHandler used to set light status
func DimmerHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	if !*ctx.Connected {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Not connected")
		return
	}
	var data DimmerPacket
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil || data.Device == "" || data.Level == "" {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}

	err = CbusDimmer(data.Device, data.Level, ctx)

	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid device")
		return
	}

	ctx.Render.JSON(w, http.StatusOK, Status{Status: "200", Message: data.Device})
}

// CurtainHandler used to set curtain
func CurtainHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	if !*ctx.Connected {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Not connected")
		return
	}
	var data DevicePacket
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil || data.Device == "" || data.State == "" {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}

	if data.State != "Position.Close" && data.State != "Position.Open" && data.State != "Position.Stop" {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}

	err = CbusCurtain(data.Device, data.State, ctx)

	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid device")
		return
	}

	ctx.Render.JSON(w, http.StatusOK, Status{Status: "200", Message: data.Device})
}

// ReportStateHandler to report state of device
func ReportStateHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	id, ok := r.URL.Query()["id"]
	if !ok || len(id[0]) < 1 {
		ctx.Render.JSON(w, http.StatusNotFound, "no device id")
		return
	}
	var data ReportState
	row := SQL.QueryRow("SELECT `categories`, `status` FROM `devices` WHERE `endpointid`=?", id[0])
	err := row.Scan(&data.Type, &data.State)
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "no device found with id")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, data)
}

// NotImplementedHandler set as root handler
func NotImplementedHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	fmt.Fprintf(w, "NotImplemented")
}

// GetDevicesHandler get devices
func GetDevicesHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	devices, err := DBGetAllDevices()
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, devices)
}

// GetDeviceHandler get device
func GetDeviceHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	vars := mux.Vars(r)
	deviceID, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid device ID")
		return
	}
	device, err := DBGetDevice(deviceID)
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, device)

}

// PostDeviceHandler edit device
func PostDeviceHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	vars := mux.Vars(r)
	deviceID, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid device ID")
		return
	}
	var data Device
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&data)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}
	data.ID = deviceID
	err = DBPostDevice(data)
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, "Edit device: "+string(deviceID))
}

// PutDeviceHandler add device
func PutDeviceHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	var data Device
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	log.Println(err)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}
	if data.Categories == "" {
		data.Categories = "LIGHT"
	}
	data.EndpointID = uuid.NewV4().String()
	if data.Categories == "LIGHT" || data.Categories == "OTHER" {
		data.Status = "ON"
	}
	data.Discovery = true
	err = DBPutDevice(data)
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, data.EndpointID)
}

// GetRoomsHandler get Rooms
func GetRoomsHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	Rooms, err := DBGetAllRooms()
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, Rooms)
}

// GetRoomHandler get Room
func GetRoomHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	vars := mux.Vars(r)
	RoomID, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid Room ID")
		return
	}
	Room, err := DBGetRoom(RoomID)
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, Room)

}

// LoginHandler get devices
func LoginHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	var data Login
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}
	dbpassword, dbroomid, dbisadmin, err := DBLogin(data.Username)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Bad password")
		return
	}
	if dbpassword != EncryptPassword(data.Password) {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Bad password")
		return
	}
	jwt, err := JWTcreate(dbroomid, dbisadmin)
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   jwt,
		Expires: time.Now().Add(6 * time.Hour),
	})
	ctx.Render.JSON(w, http.StatusOK, "Logged in")
}

// LogoutHandler logout login
func LogoutHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   "",
		Expires: time.Now(),
	})
	ctx.Render.JSON(w, http.StatusOK, "Logged out")
}

// CreateUserHandler get devices
func CreateUserHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	var data EditUser
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&data)
	if err != nil {
		ctx.Render.JSON(w, http.StatusMethodNotAllowed, "Invalid request body")
		return
	}
	_, err = DBAddUser(data)
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, "Inserted User: "+data.Username)
}

// GetUsersHandler get all users
func GetUsersHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	users, err := DBGetUsers()
	if err != nil {
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	ctx.Render.JSON(w, http.StatusOK, users)
}

// GetClientConfigHandler get config for go-cbus-client
func GetClientConfigHandler(w http.ResponseWriter, r *http.Request, ctx AppContext) {
	uuid, err := DBAddUser(EditUser{
		Username: uuid.NewV4().String(),
		Password: uuid.NewV4().String(),
	})
	if err != nil {
		log.Println(err)
		ctx.Render.JSON(w, http.StatusInternalServerError, "Database err")
		return
	}
	AccessToken[uuid] = true
	config := Config{
		Cbus: struct {
			Address string `json:"address"`
			Port    string `json:"port"`
			Status  string `json:"status"`
		}{
			Address: "127.0.0.1",
			Port:    "20023",
			Status:  "20025",
		},
		SSL:    true,
		Server: "www.tsatech.nz",
		Path:   "/cbus",
		WSpath: "/ws",
		Token:  uuid,
	}
	ctx.Render.JSON(w, http.StatusOK, config)
}
