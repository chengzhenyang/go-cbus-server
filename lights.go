package main

import (
	"fmt"
	"log"
)

// CbusLight control lighting using websockets
func CbusLight(device string, state string, ctx AppContext) error {
	var projectname string
	var cniaddress, deviceaddress int64
	var reverse bool
	row := SQL.QueryRow("SELECT `projectname`,`cniaddress`,`deviceaddress`,`reverse` FROM `devices` WHERE (`categories`='LIGHT' OR `categories`='OTHER' OR `categories`='DIMMER') AND `endpointid`=?", device)
	err := row.Scan(&projectname, &cniaddress, &deviceaddress, &reverse)
	if err != nil {
		return err
	}
	log.Println(state)

	if reverse {
		switch state {
		case "ON":
			state = "OFF"
		case "OFF":
			state = "ON"
		}
	}

	return WSLightsend(state, fmt.Sprintf("//%s/%d/56/%d", projectname, cniaddress, deviceaddress), ctx)
}

// CbusDimmer control lighting using websockets
func CbusDimmer(device string, level string, ctx AppContext) error {
	var projectname string
	var cniaddress, deviceaddress int64
	row := SQL.QueryRow("SELECT `projectname`,`cniaddress`,`deviceaddress` FROM `devices` WHERE (`categories`='LIGHT' OR `categories`='OTHER' OR `categories`='DIMMER') AND `endpointid`=?", device)
	err := row.Scan(&projectname, &cniaddress, &deviceaddress)
	if err != nil {
		return err
	}

	if level == "0" {
		return WSLightsend("OFF", fmt.Sprintf("//%s/%d/56/%d", projectname, cniaddress, deviceaddress), ctx)
	} else if level == "100" {
		return WSLightsend("ON", fmt.Sprintf("//%s/%d/56/%d", projectname, cniaddress, deviceaddress), ctx)
	}

	return WSDimmersend(level, fmt.Sprintf("//%s/%d/56/%d", projectname, cniaddress, deviceaddress), ctx)
}

// WSLightsend to ws using address and return error
func WSLightsend(state string, address string, ctx AppContext) error {
	for auth := range WS {
		err := WS[auth].Send(WSpacket{Type: "c", Message: state + " " + address}, ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

// WSDimmersend to ws using address and return error
func WSDimmersend(level string, address string, ctx AppContext) error {
	for auth := range WS {
		err := WS[auth].Send(WSpacket{Type: "c", Message: "RAMP " + address + " " + level + "%"}, ctx)
		if err != nil {
			return err
		}
	}
	return nil
}
